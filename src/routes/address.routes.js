import { Router } from "express";
import * as addressController from "../controllers/address.controller";

const router = Router();

router.post("/", addressController.createAddress);
router.get("/:id", addressController.getAddress);
router.put("/:id", addressController.updateAddress);
router.delete("/:id", addressController.deleteAddress);

export default router;