import { Router } from "express";
import * as OrderController from "../controllers/order.controller";

const router = Router();

router.post("/", OrderController.addOrder);
router.get("/:id", OrderController.getOrder);
router.get("/", OrderController.getAllOrders);
router.put("/:id", OrderController.updateOrder);

router.delete("/:id", OrderController.deleteOrder);
router.get("/buyer/:id", OrderController.getOrdersByBuyer);
router.get("/status/:status", OrderController.getOrdersByStatus);
router.patch(":id/status", OrderController.updateOrderStatus );
router.get("/seller/:refSeller", OrderController.getOrderBySeller);

export default router;