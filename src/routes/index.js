'use strict';

import { Router } from 'express';
import * as ctrlValue from '../controllers';



const router = Router();

router.post('/api/value', ctrlValue.addValue);

router.get('/api/value', ctrlValue.getValue);

router.get('/api/value/:id', ctrlValue.getValueById);

router.put('/api/value/:id', ctrlValue.updateValue);

router.delete('/api/value/:id', ctrlValue.deleteValue);

export default router;
