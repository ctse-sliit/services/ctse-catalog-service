import { Router } from "express";
import * as ItemController from "../controllers/item.controller";

const router = Router();

router.post("/", ItemController.createItem);
router.get("/:id", ItemController.getItems);
router.put("/:id", ItemController.updateItem);
router.get("/", ItemController.getAllItems);
router.delete("/:id", ItemController.deleteItem);

export default router;