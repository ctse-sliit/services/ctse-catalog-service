import { Router } from "express";
import * as cartController from "../controllers/cart.controller";

const router = Router();

router.post("/", cartController.createCart);
router.get("/:id", cartController.getCartByBuyer);
router.put("/addItem/:id", cartController.addItemsToCart);
router.put("/updateQuantity/:id", cartController.updateQuantity);
router.put("/removeItem/:id", cartController.deleteItemFromCart);
router.put("/clear/:id", cartController.clearCart);
router.delete("/:id", cartController.deleteCart);

export default router;