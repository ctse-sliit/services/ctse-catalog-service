const cluster_services = {

    'herbland-keyclaok': 'http://keycloak:8080',
    'herbland-bff' : 'http://herbland-bff:3001',
    'herbland-catalog-mgt-service' : 'http://herbland-catalog-mgt-service:3001',
    'herbland-db-service': 'http://herbland-db-service:3001',
    'herbland-notification-mgt-service': 'http://herbland-notification-mgt-service:3001',
    'herbland-payment-mgt-service': 'http://herbland-payment-mgt-service:3001',
    'herbland-user-mgt-service': 'http://herbland-user-mgt-service:3001'

}

const local_services = {

    'herbland-keyclaok': 'https://monday.lk/auth',
    'herbland-bff' : 'https://api.monday.lk',
    'herbland-catalog-mgt-service' : 'https://api.monday.lk',
    'herbland-db-service': 'https://api.monday.lk',
    'herbland-notification-mgt-service': 'https://api.monday.lk',
    'herbland-payment-mgt-service': 'https://api.monday.lk',
    'herbland-user-mgt-service': 'https://api.monday.lk'

}

export const getConfig = () => {
    
    if(process.env.ENVIRONMENT == 'cluster'){
        return cluster_services;
    }
    else{
        return local_services;
    }
}

// Service names
// use this names in API calls

// herbland-keyclaok
// herbland-bff
// herbland-catalog-mgt-service
// herbland-db-service
// herbland-notification-mgt-service
// herbland-payment-mgt-service
// herbland-user-mgt-service

