import axios from 'axios';
import { getConfig } from '../utils/k8s.service.discovery';

const pathResolver = getConfig();

export const createItem = async (req,res) => {
    const {name, refSeller, price, quantity, images, description, keywords, category, bestBy} = req.body;
    console.log(req.body);
    try {
        const response = await axios.post(`${process.env.DB_BASE_URL}/api/db-service/item`, {name, refSeller, price, quantity, images, description, keywords, category, bestBy});
        if (response) {
            res.status(200).json(response.data);
        }
        else {
            console.log('asd');
            res.status(404).send({ message: response.message, status: 'failure' });
        }
    }
    catch (err) {
        res.status(404).send({ message: err, status: 'failure' });
    }
}

export const getItems = async (req,res) => {
    const id = req.params.id;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/item/${id}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const updateItem = async (req,res) => {
    const id = req.params.id;
    const {name, refSeller, price, quantity, images, description, keywords, category, bestBy} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/item/${id}`, {name, refSeller, price, quantity, images, description, keywords, category, bestBy});
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const getAllItems = async (req,res) => {
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/item`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}

export const deleteItem = async (req,res) => {
    const id = req.params.id;
    try {
        const response = await axios.delete(`${process.env.DB_BASE_URL}/api/db-service/item/${id}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: 'failure' });
    }
}