import axios from "axios";
import { getConfig } from '../utils/k8s.service.discovery';
const pathResolver = getConfig();

export const addOrder = async (req, res) => {
    try {
        const { refBuyer,address, itemList, tracking,grossPrice, deliveryCharge, serviceFee, bankPaymentCharge, totalPrice } = req.body;
        const refSeller =[];
        for (let i = 0; i < itemList.length; i++) {
            refSeller.push(itemList[i].refSeller);
          }
        const {firstName, lastName, email, addressLine1, addressLine2, city, state, country, postalCode, billingMobile} = address;
        const resAddress = await axios.post(`${process.env.DB_BASE_URL}/api/db-service/address`, {firstName, lastName, email, addressLine1, addressLine2, city, state, country, postalCode, billingMobile});
        const refAddress = resAddress.data.data._id;
        const response = await axios.post(`${process.env.DB_BASE_URL}/api/db-service/order`, { refBuyer, refSeller, refAddress, itemList,tracking, grossPrice, deliveryCharge, serviceFee, bankPaymentCharge, totalPrice });
        const id=refBuyer;
        await axios.put(`${process.env.DB_BASE_URL}/api/db-service/cart/clear/${id}`);
        await axios.patch(`${process.env.DB_BASE_URL}/api/db-service/item/`,{itemList});
        console.log(response);
        res.status(200).json(response.data);
    } catch (error) {
         console.error(error);
        res.status(500).json({ message: "Something went wrong!" });
    }
};


export const getOrder = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/order/${id}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const updateOrder = async (req, res) => {
    const id = req.params.id;
    const {refBuyer, refSeller, refAddress, itemList, grossPrice, deliveryCharge, serviceFee, bankPaymentCharge, totalPrice} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/order/${id}`, {refBuyer, refSeller, refAddress, itemList, grossPrice, deliveryCharge, serviceFee, bankPaymentCharge, totalPrice});
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const deleteOrder = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.delete(`${process.env.DB_BASE_URL}/api/db-service/order/${id}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const getOrdersByBuyer = async (req, res) => {
    const refBuyer = req.params.id;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/order/buyer/${refBuyer}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const getOrdersByStatus = async (req, res) => {
    const status = req.params.status;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/order/status/${status}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const getOrderBySeller = async (req, res) => {
    const refSeller = req.params.refSeller;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/order/seller/${refSeller}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const updateOrderStatus = async (req, res) => {
    const id = req.params.id;
    const {status} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/order/${id}/status`, {status});
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const getAllOrders = async (req, res) => {
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/order`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
};