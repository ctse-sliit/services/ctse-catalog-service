import axios from "axios";

export const createAddress = async (req, res) => {
    const {firstName, lastName, email, addressLine1, addressLine2, city, state, country, postalCode, billingMobile} = req.body;
    try {
        const response = await axios.post(`${process.env.DB_BASE_URL}/api/db-service/address`, {firstName, lastName, email, addressLine1, addressLine2, city, state, country, postalCode, billingMobile});
        res.status(200).json(response.data)
    }
    catch(error) {
        res.status(500).json(error.message);
    }
}

export const getAddress = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/address/${id}`);
        res.status(200).json(response.data)
    }
    catch(error) {
        res.status(500).json(error.message);
    }
}

export const updateAddress = async (req, res) => {
    const id = req.params.id;
    const {firstName, lastName, email, addressLine1, addressLine2, city, state, country, postalCode, billingMobile} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/address/${id}`, {firstName, lastName, email, addressLine1, addressLine2, city, state, country, postalCode, billingMobile});
        res.status(200).json(response.data)
    }
    catch(error) {
        res.status(500).json(error.message);
    }
}

export const deleteAddress = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.delete(`${process.env.DB_BASE_URL}/api/db-service/address/${id}`);
        res.status(200).json(response.data)
    }
    catch(error) {
        res.status(500).json(error.message);
    }
}

