import axios from "axios";

import { getConfig } from '../utils/k8s.service.discovery';

const pathResolver = getConfig();

export const createCart = async (req, res) => {
    const {buyerRef, item} = req.body;
    try {
        const response = await axios.post(`${process.env.DB_BASE_URL}/api/db-service/cart`, {buyerRef, item});
            res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const getCartByBuyer = async (req, res) => { 
    const id = req.params.id;
    try {
        const response = await axios.get(`${process.env.DB_BASE_URL}/api/db-service/cart/${id}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
};

export const addItemsToCart = async (req, res) => {
    const id = req.params.id;
    const {item, quantity} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/cart/addItem/${id}`, {item,quantity});
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const updateQuantity = async (req, res) => {
    const id = req.params.id;
    const {item, quantity} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/cart/updateQuantity/${id}`, {item,quantity});
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const deleteItemFromCart = async (req, res) => {
    const id = req.params.id;
    const {item} = req.body;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/cart/removeItem/${id}`, {item});
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const clearCart = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.put(`${process.env.DB_BASE_URL}/api/db-service/cart/clear/${id}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
}

export const deleteCart = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.delete(`${process.env.DB_BASE_URL}/api/db-service/cart/${id}`);
        res.status(200).json(response.data);
    }
    catch (err) {
        res.status(404).send({ message: err.message, status: "failure" });
    }
};
