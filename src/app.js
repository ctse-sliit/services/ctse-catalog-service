'use strict';

import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import 'dotenv/config';
import mainRoutes from './routes';
import itemRoutes from './routes/item.routes';
import addressRoutes from './routes/address.routes';
import orderRoutes from './routes/order.routes'
import cartRoutes from './routes/cart.routes';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

if (process.env.NODE_ENV === 'development') {
  app.use(require('morgan')('dev'));
}

app.use(helmet());
app.use(express.static(__dirname + '/src/public'));

/* Routes */

// This context path unique to this service
const contextPath = "/api/catalog-mgt-service"

app.get(`${contextPath}/health`, (req, res) => {
  const data = {
    uptime: process.uptime(),
    message: 'Ok',
    date: new Date()
  }

  res.status(200).send(data);
});

app.use(contextPath, mainRoutes);
app.use(`${contextPath}/item`, itemRoutes);
app.use(`${contextPath}/address`, addressRoutes);
app.use(`${contextPath}/order`, orderRoutes);
app.use(`${contextPath}/cart`, cartRoutes);

export {app, contextPath};